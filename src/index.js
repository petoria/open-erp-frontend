import React from "react";
import ReactDOM from "react-dom";
import {createBrowserHistory} from "history";
import {Router, Route, Switch} from "react-router-dom";
import "Site/assets/scss/material-kit-react.scss?v=1.4.0";
import Components from "Site/views/Components/Components.jsx";
import HomePage from "Site/views/HomePage/HomePage";
import PricingPage from "./Site/views/PricingPage/PricingPage";
import LoginPage from "./Site/views/LoginPage/LoginPage";
import ChiSiamoPage from "./Site/views/ChiSiamoPage/ChiSiamoPage";
import DashboardPage from "./Dashboard/layouts/Admin";
import Departments from "./Dashboard/layouts/Departments";
import Products from "./Dashboard/layouts/Products";
import Orders from "./Dashboard/layouts/Orders";
import Clients from "./Dashboard/layouts/Clients";
import NewOrdine from "./Dashboard/layouts/NewOrdine";

import i18n from "i18next";
import {useTranslation, initReactI18next} from "react-i18next";

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources: {
            en: {
                translation: {
                    "Welcome to React": "Welcome to React and react-i18next"
                }
            }
        },
        lng: "en",
        fallbackLng: "en",

        interpolation: {
            escapeValue: false
        }
    });


let hist = createBrowserHistory();

ReactDOM.render(
    <Router history={hist}>
        <Switch>
            <Route
                path="/components"
                component={Components}
            />
            <Route
                path="/pricing"
                component={PricingPage}
            />
            <Route
                path="/chi-siamo"
                component={ChiSiamoPage}
            />
            <Route
                path="/login"
                component={LoginPage}
            />
            <Route
                path="/admin/dashboard"
                component={DashboardPage}
            />
            <Route
                path="/admin/deps"
                component={Departments}
            />
            <Route
                path="/admin/products"
                component={Products}
            />
            <Route
                path="/admin/orders"
                component={Orders}
            />
            <Route
                path="/admin/clients"
                component={Clients}
            />
            <Route
                path="/admin/newOrdine"
                component={NewOrdine}
            />
            <Route
                path="/"
                component={HomePage}
            />
        </Switch>
    </Router>,
    document.getElementById("root")
);