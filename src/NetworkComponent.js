import React from "react";

export default class NetworkComponent extends React.Component{

    componentWillMount() {
        this.loadState();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    constructor(props){
        super(props);
        this.auth = undefined;
        this.dns = "";
        this.port = 80;
    }

    loadState(){

        if(window.isLoggedInAPI){
            this.dns = window.dns;
            this.port = window.port;
            this.auth = window.token;
            return;
        } else if(window.isLogged){
            this.auth = window.token;
            return;
        }

        try{
            let data = JSON.parse(document.cookie);
            window.isLogged = data.logged;
            window.token = data.token;
            this.auth = data.token;
            window.username = data.username;
            window.sessions = data.sessions;
        } catch (e) {}
    }

    _saveState(token, username, sessions, logged){
        this.auth = token;

        document.cookie = JSON.stringify({
            logged: logged,
            token: this.auth,
            username: username,
            sessions: sessions
        });

        window.isLogged = logged;
        window.token = this.auth;
        window.username = username;
        window.sessions = sessions;
    }

    _saveStateAPI(token, dns, port){
        this.auth = token;
        this.dns = dns;
        this.port = port;

        window.isLoggedInAPI = true;
        window.token = token;
        window.dns = dns;
        window.port = port;
    }

    async GET(url, headers={}, useAuth=true){
        return await this.REQUEST("GET", url, undefined, headers, useAuth);
    }

    async POST(url, body, headers={}, useAuth=true){
        return await this.REQUEST("POST", url, body, headers, useAuth);
    }

    async REQUEST(verb, url, body, headers={}, useAuth=true){

        console.log(this.auth)

        if(this.dns){
            url = `${this.dns}:${this.port}${url}`
        }

        let head = headers;
        if(body) {
            head['Accept'] = 'application/json';
            head['Content-Type'] = 'application/json';
        }
        if(useAuth && (window.isLogged || window.isLoggedInAPI)) head['Authentication'] = this.auth;

        let res = await fetch(url, {method: verb, headers: head, body: JSON.stringify(body)});
        return {
            code: res.status,
            data: await res.json(),
            res
        };
    }
}