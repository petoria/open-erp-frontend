/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import withStyles from "@material-ui/core/styles/withStyles";
import Footer from "Dashboard/components/Footer/Footer.jsx";
import Sidebar from "Dashboard/components/Sidebar/Sidebar.jsx";
import routes from "Dashboard/routes.js";
import dashboardStyle from "Dashboard/assets/jss/material-dashboard-react/layouts/dashboardStyle.jsx";
import image from "Dashboard/assets/img/sidebar-2.jpg";
import logo from "Dashboard/assets/img/logo_rounded.ico";
import NetworkComponent from "../../NetworkComponent";
import Card from "../../Site/components/Card/Card";
import CardHeader from "../../Site/components/Card/CardHeader";
import CardBody from "../../Site/components/Card/CardBody";
import Table from "../components/Table/Table";
import Button from "../components/CustomButtons/Button";
import TextField from "@material-ui/core/TextField";
import Delete from "@material-ui/icons/Delete";
import Input from "@material-ui/icons/Input";
import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

class NewOrdine extends NetworkComponent {
    constructor(props) {
        super(props);
        this.state = {
            image: image,
            color: "blue",
            hasImage: true,
            fixedClasses: "dropdown show",
            mobileOpen: false,
            tableData: [],
            state: "normal",
            order: {
                client: {
                    name: "",
                    value: ""
                },
                data: "",
                products: []
            }
        };

        this.clients = [];
        this.products = [];
        this.chProducts = ["", 1, 1];
    }

    handleImageClick = image => {
        this.setState({image: image});
    };
    handleColorClick = color => {
        this.setState({color: color});
    };
    handleFixedClick = () => {
        if (this.state.fixedClasses === "dropdown") {
            this.setState({fixedClasses: "dropdown show"});
        } else {
            this.setState({fixedClasses: "dropdown"});
        }
    };
    handleDrawerToggle = () => {
        this.setState({mobileOpen: !this.state.mobileOpen});
    };

    getRoute() {
        return this.props.location.pathname !== "/admin/maps";
    }

    resizeFunction = () => {
        if (window.innerWidth >= 960) {
            this.setState({mobileOpen: false});
        }
    };

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            const ps = new PerfectScrollbar(this.refs.mainPanel);
        }
        window.addEventListener("resize", this.resizeFunction);

        this.loadState();

        this.GET("/clients").then(res => {
            if (!res.data.list) {
                alert("ERRORE");
                return;
            }

            res.data.list.forEach((e) => this.clients.push({value: e.cf, name: e.name}));
            this.forceUpdate();
        });

        this.GET("/prods").then(res => {
            if (!res.data.list) {
                alert("ERRORE");
                return;
            }

            res.data.list.forEach((e) => this.products.push({name: e.desc, price: e.price, code: e.code}));
            this.forceUpdate();
        });
    }

    componentDidUpdate(e) {
        if (e.history.location.pathname !== e.location.pathname) {
            this.refs.mainPanel.scrollTop = 0;
            if (this.state.mobileOpen) {
                this.setState({mobileOpen: false});
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeFunction);
    }

    orderDataIsCorrect() {
        return this.state.order.data.charAt(2) === "/" && this.state.order.data.charAt(5) === "/" && this.state.order.data.length === 10;
    }

    sendDisabled() {
        return !(this.state.order.client.name && (this.state.order.data && this.orderDataIsCorrect()) && this.state.order.products.length > 0);
    }

    getTableData() {
        let tableData = [];

        this.state.order.products.forEach((e) => {
            tableData.push([this.products[e[0].value].name, e[1], e[2]]);
        });

        return tableData;
    }

    addProducts() {
        this.state.order.products = [...this.state.order.products, [this.chProducts[0], this.chProducts[1], this.chProducts[2], ""]];
        console.log(this.state.order.products)
        this.forceUpdate();
    }

    render() {
        const {classes, ...rest} = this.props;
        return (
            <div className={classes.wrapper}>
                <Sidebar
                    routes={routes}
                    logoText={"PETORIA"}
                    logo={logo}
                    image={this.state.image}
                    handleDrawerToggle={this.handleDrawerToggle}
                    open={this.state.mobileOpen}
                    color={this.state.color}
                    {...rest}
                />
                <div className={classes.mainPanel} ref="mainPanel">
                    <div className={classes.container}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={classes.cardTitleWhite}>
                                    Nuovo ordine
                                </h4>
                                <p className={classes.cardCategoryWhite}>
                                    Crea un nuovo ordine cliente
                                </p>
                                <Button style={{backgroundColor: "#ffcc00", color: "black"}}
                                        disabled={this.sendDisabled()} onClick={() => {

                                    let ob = {};
                                    this.products.forEach(e => ob[e.name] = e.code);
                                    let order = [];
                                    this.state.order.products.map(e => {
                                        order.push({code: ob[this.products[e[0].value].name], qta: e[1]})
                                    });

                                    this.POST("/orders", {client: this.state.order.client, products: order}).then(e => {

                                    })

                                }}>
                                    Invia
                                </Button>
                            </CardHeader>
                            <CardBody>
                                <GridContainer>
                                    <GridItem xs={1} style={{textAlign: "right"}}>
                                        <h4>Cliente:</h4>
                                    </GridItem>
                                    <GridItem xs={2}>
                                        <FormControl required className={classes.formControl}>
                                            <InputLabel htmlFor="client-required">Cliente</InputLabel>
                                            <Select
                                                value={this.state.order.client.value}
                                                onChange={(e) => {
                                                    this.state.order.client = e.target;
                                                    this.forceUpdate();
                                                }}
                                                name="client"
                                                inputProps={{
                                                    id: 'client-required',
                                                }}
                                                className={classes.selectEmpty}
                                            >
                                                {this.clients.map(e => {
                                                    return (
                                                        <MenuItem value={e.value}>{e.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                            <FormHelperText>Richiesto</FormHelperText>
                                        </FormControl>
                                    </GridItem>
                                    <GridItem xs={2} style={{textAlign: "right"}}>
                                        <h4>Data di consegna:</h4>
                                    </GridItem>
                                    <GridItem xs={3}>
                                        <TextField required onChange={(e) => this.state.order.data = e.target.value}
                                                   label={"Data di consegna"} placeholder={"dd/MM/yyyy"}
                                                   helperText={"Richiesto"}/>
                                    </GridItem>
                                    <GridItem xs={12}>
                                        <Table
                                            tableHeaderColor="primary"
                                            tableHead={["Prodotto", "Quantità", "Prezzo", ""]}
                                            tableData={[...this.getTableData().map((e, i) =>
                                                [...e.slice(0, 3),
                                                    <Button style={{backgroundColor: "red"}}
                                                            onClick={() => this.delProduct(i)}>
                                                        <Delete/>
                                                    </Button>]),
                                                [
                                                    <FormControl className={classes.formControl}>
                                                        <InputLabel htmlFor="products-required">Prodotto</InputLabel>
                                                        <Select
                                                            value={this.chProducts[0].value}
                                                            onChange={(e) => {
                                                                this.chProducts[0] = e.target;
                                                                this.chProducts[2] = this.chProducts[1] * (this.chProducts[0] === "" ? 0 : this.products[this.chProducts[0].value].price);
                                                                this.forceUpdate();
                                                            }}
                                                            name="products"
                                                            inputProps={{
                                                                id: 'products-required',
                                                            }}
                                                            className={classes.selectEmpty}
                                                        >
                                                            {this.products.map((e, i) => {
                                                                return (
                                                                    <MenuItem value={i}>{e.name}</MenuItem>
                                                                );
                                                            })}
                                                        </Select>
                                                        <FormHelperText>Richiesto</FormHelperText>
                                                    </FormControl>,
                                                    <TextField
                                                        id="standard-number"
                                                        label="Quantità"
                                                        value={this.chProducts[1]}
                                                        onChange={(e) => {
                                                            if (e.target.value > 0) {
                                                                this.chProducts[1] = e.target.value;
                                                                this.chProducts[2] = e.target.value * (this.chProducts[0] === "" ? 0 : this.products[this.chProducts[0].value].price);
                                                                this.forceUpdate();
                                                            }
                                                        }}
                                                        type="number"
                                                        className={classes.textField}
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        margin="normal"
                                                    />,
                                                    <TextField
                                                        disabled
                                                        value={this.chProducts[0] === "" ? 0 : this.chProducts[1] === 1 ? this.products[this.chProducts[0].value].price : this.chProducts[2]}
                                                        className={classes.textField}
                                                    />,
                                                    <Button style={{backgroundColor: "green"}}
                                                            onClick={() => this.addProducts()}>
                                                        <Input/>
                                                    </Button>
                                                ]]}
                                        />
                                    </GridItem>
                                </GridContainer>
                            </CardBody>
                        </Card>
                    </div>
                    <Footer/>
                </div>
            </div>
        );
    }
}

NewOrdine.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(NewOrdine);
