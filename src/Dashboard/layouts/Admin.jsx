/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import withStyles from "@material-ui/core/styles/withStyles";
import Footer from "Dashboard/components/Footer/Footer.jsx";
import Sidebar from "Dashboard/components/Sidebar/Sidebar.jsx";
import routes from "Dashboard/routes.js";
import dashboardStyle from "Dashboard/assets/jss/material-dashboard-react/layouts/dashboardStyle.jsx";
import image from "Dashboard/assets/img/sidebar-2.jpg";
import logo from "Dashboard/assets/img/logo_rounded.ico";
import NetworkComponent from "../../NetworkComponent";
import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";
import Card from "Dashboard/components/Card/Card.jsx";
import CardHeader from "Dashboard/components/Card/CardHeader.jsx";
import CardIcon from "Dashboard/components/Card/CardIcon.jsx";
import CardFooter from "Dashboard/components/Card/CardFooter.jsx";
import Monetization from "@material-ui/icons/MonetizationOn";
import Update from "@material-ui/icons/Update";
import ChartistGraph from "react-chartist";
import AccessTime from "@material-ui/core/SvgIcon/SvgIcon";
import CardBody from "../components/Card/CardBody";

class Dashboard extends NetworkComponent {
    constructor(props) {
        super(props);
        this.state = {
            image: image,
            color: "blue",
            hasImage: true,
            fixedClasses: "dropdown show",
            mobileOpen: false,
            numOrdini: 5,
            // allDeps: {
            //     // data: {
            //     //     labels: [
            //     //         "CF",
            //     //         "IF",
            //     //         "AF1",
            //     //         "SLDMNT1",
            //     //         "AF2",
            //     //         "SLDMNT2",
            //     //         "MC",
            //     //         "V"
            //     //     ],
            //     //     series: [[542, 443, 320, 780, 553, 453, 326, 434]]
            //     // },
            //     data: {
            //         labels: [
            //             "Jan",
            //             "Feb",
            //             "Mar",
            //             "Apr",
            //             "Mai",
            //             "Jun",
            //             "Jul",
            //             "Aug",
            //             "Sep",
            //             "Oct",
            //             "Nov",
            //             "Dec"
            //         ],
            //         series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
            //     },
            //     options: {
            //         axisX: {
            //             showGrid: false
            //         },
            //         low: 0,
            //         high: 1000,
            //         chartPadding: {
            //             top: 0,
            //             right: 5,
            //             bottom: 0,
            //             left: 0
            //         }
            //     },
            //     responsiveOptions: [
            //         [
            //             "screen and (max-width: 640px)",
            //             {
            //                 seriesBarDistance: 5,
            //                 axisX: {
            //                     labelInterpolationFnc: function(value) {
            //                         return value[0];
            //                     }
            //                 }
            //             }
            //         ]
            //     ],
            //     animation: {
            //         draw: function(data) {
            //             if (data.type === "bar") {
            //                 data.element.animate({
            //                     opacity: {
            //                         begin: (data.index + 1) * 80,
            //                         dur: 500,
            //                         from: 0,
            //                         to: 1,
            //                         easing: "ease"
            //                     }
            //                 });
            //             }
            //         }
            //     }
            // },
        };
    }

    handleImageClick = image => {
        this.setState({image: image});
    };
    handleColorClick = color => {
        this.setState({color: color});
    };
    handleFixedClick = () => {
        if (this.state.fixedClasses === "dropdown") {
            this.setState({fixedClasses: "dropdown show"});
        } else {
            this.setState({fixedClasses: "dropdown"});
        }
    };
    handleDrawerToggle = () => {
        this.setState({mobileOpen: !this.state.mobileOpen});
    };

    getRoute() {
        return this.props.location.pathname !== "/admin/maps";
    }

    resizeFunction = () => {
        if (window.innerWidth >= 960) {
            this.setState({mobileOpen: false});
        }
    };

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            const ps = new PerfectScrollbar(this.refs.mainPanel);
        }
        window.addEventListener("resize", this.resizeFunction);

        // this.GET("/loads").then(res => {
        //     console.log(res)
        // });
    }

    componentDidUpdate(e) {
        if (e.history.location.pathname !== e.location.pathname) {
            this.refs.mainPanel.scrollTop = 0;
            if (this.state.mobileOpen) {
                this.setState({mobileOpen: false});
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeFunction);
    }

    render() {
        const {classes, ...rest} = this.props;
        return (
            <div className={classes.wrapper}>
                <Sidebar
                    routes={routes}
                    logoText={"PETORIA"}
                    logo={logo}
                    image={this.state.image}
                    handleDrawerToggle={this.handleDrawerToggle}
                    open={this.state.mobileOpen}
                    color={this.state.color}
                    {...rest}
                />
                <div className={classes.mainPanel} ref="mainPanel">
                    <div className={classes.container}>
                        <GridContainer>
                            {/*<GridItem xs={9}>*/}
                            {/*    <Card chart>*/}
                            {/*        <CardHeader color="warning">*/}
                            {/*            <ChartistGraph*/}
                            {/*                className="ct-chart"*/}
                            {/*                data={this.state.allDeps.data}*/}
                            {/*                type="Bar"*/}
                            {/*                options={this.state.allDeps.options}*/}
                            {/*                responsiveOptions={this.state.allDeps.responsiveOptions}*/}
                            {/*                listener={this.state.allDeps.animation}*/}
                            {/*            />*/}
                            {/*        </CardHeader>*/}
                            {/*        <CardBody>*/}
                            {/*            <h4 className={classes.cardTitle}>Carico dei reparti</h4>*/}
                            {/*            <p className={classes.cardCategory}>*/}
                            {/*                Visualizza il carico di lavoro di ogni reparto per questa giornata*/}
                            {/*            </p>*/}
                            {/*        </CardBody>*/}
                            {/*        <CardFooter chart>*/}
                            {/*            <div className={classes.stats}>*/}
                            {/*                <Update/>*/}
                            {/*                Aggiornato adesso*/}
                            {/*            </div>*/}
                            {/*        </CardFooter>*/}
                            {/*    </Card>*/}
                            {/*</GridItem>*/}
                            <GridItem xs={3}>
                                <Card>
                                    <CardHeader color="info" stats icon>
                                        <CardIcon color="info">
                                            <Monetization/>
                                        </CardIcon>
                                        <p className={classes.cardCategory}>Ordini cliente in corso</p>
                                        <h3 className={classes.cardTitle}>{this.state.numOrdini}</h3>
                                    </CardHeader>
                                    <CardFooter stats>
                                        <div className={classes.stats}>
                                            <Update/>
                                            Aggiornato adesso
                                        </div>
                                    </CardFooter>
                                </Card>
                            </GridItem>
                        </GridContainer>
                    </div>

                    <Footer/>
                </div>
            </div>
        );
    }
}

Dashboard.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
