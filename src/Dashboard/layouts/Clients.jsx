/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import withStyles from "@material-ui/core/styles/withStyles";
import Footer from "Dashboard/components/Footer/Footer.jsx";
import Sidebar from "Dashboard/components/Sidebar/Sidebar.jsx";
import routes from "Dashboard/routes.js";
import dashboardStyle from "Dashboard/assets/jss/material-dashboard-react/layouts/dashboardStyle.jsx";
import image from "Dashboard/assets/img/sidebar-2.jpg";
import logo from "Dashboard/assets/img/logo_rounded.ico";
import NetworkComponent from "../../NetworkComponent";
import Card from "../../Site/components/Card/Card";
import CardHeader from "../../Site/components/Card/CardHeader";
import CardBody from "../../Site/components/Card/CardBody";
import Table from "../components/Table/Table";
import Button from "../components/CustomButtons/Button";
import TextField from "@material-ui/core/TextField";
import Delete from "@material-ui/icons/Delete";
import Input from "@material-ui/icons/Input";

class Clients extends NetworkComponent {
    constructor(props) {
        super(props);
        this.state = {
            image: image,
            color: "blue",
            hasImage: true,
            fixedClasses: "dropdown show",
            mobileOpen: false,
            tableData: [],
            state: "normal",
        };

        this.chData = [];
    }

    handleImageClick = image => {
        this.setState({image: image});
    };
    handleColorClick = color => {
        this.setState({color: color});
    };
    handleFixedClick = () => {
        if (this.state.fixedClasses === "dropdown") {
            this.setState({fixedClasses: "dropdown show"});
        } else {
            this.setState({fixedClasses: "dropdown"});
        }
    };
    handleDrawerToggle = () => {
        this.setState({mobileOpen: !this.state.mobileOpen});
    };

    getRoute() {
        return this.props.location.pathname !== "/admin/maps";
    }

    resizeFunction = () => {
        if (window.innerWidth >= 960) {
            this.setState({mobileOpen: false});
        }
    };

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            const ps = new PerfectScrollbar(this.refs.mainPanel);
        }
        window.addEventListener("resize", this.resizeFunction);

        this.loadState();
        this.GET("/clients").then(res => {
            console.log(res);
            if (!res.data.list) {
                alert("ERRORE");
                return;
            }

            console.log(res)

            this.setState({tableData: res.data.list.map(e => [e.name, e.cf, e.address, e.notes, ""])})
        })
    }

    componentDidUpdate(e) {
        if (e.history.location.pathname !== e.location.pathname) {
            this.refs.mainPanel.scrollTop = 0;
            if (this.state.mobileOpen) {
                this.setState({mobileOpen: false});
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeFunction);
    }

    addProducts() {
        this.POST("/clients", {
            name: this.chData[0],
            cf: this.chData[1],
            address: this.chData[2],
            notes: this.chData[3],
            trust: 10
        })
            .then(res => {
                if (res.code === 200)
                    this.setState({tableData: [...this.state.tableData, [this.chData[0], this.chData[1], this.chData[2], this.chData[3], ""]]})
                else
                    alert("ERRORE");
            })
            .catch((e) => {
                console.log(e);
                alert("ERRORE");
            })
    }

    delProduct(i) {
        this.REQUEST("DELETE", `/clients/${this.state.tableData[i][1]}`, {code: this.state.tableData[i][1]}).then(res => {
            if (res.code === 200) {
                this.state.tableData.splice(i, 1);
                this.forceUpdate();
            } else
                alert("ERRORE");
        });
    }

    render() {
        const {classes, ...rest} = this.props;
        return (
            <div className={classes.wrapper}>
                <Sidebar
                    routes={routes}
                    logoText={"PETORIA"}
                    logo={logo}
                    image={this.state.image}
                    handleDrawerToggle={this.handleDrawerToggle}
                    open={this.state.mobileOpen}
                    color={this.state.color}
                    {...rest}
                />
                <div className={classes.mainPanel} ref="mainPanel">
                    <div className={classes.container}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={classes.cardTitleWhite}>
                                    Clienti
                                </h4>
                                <p className={classes.cardCategoryWhite}>
                                    Elenco di tutti i clienti
                                </p>
                                <Button style={{backgroundColor: "#ffcc00", color: "black"}} onClick={() => {
                                    this.setState({state: (this.state.state === "normal" ? "edit" : "normal")})
                                }}>
                                    {this.state.state === "normal" ? "Modifica" : "Salva"}
                                </Button>
                            </CardHeader>
                            <CardBody>
                                <Table
                                    tableHeaderColor="primary"
                                    tableHead={["Nome", "Codice fiscale", "Indirizzo", "Note", ""]}
                                    tableData={this.state.state === "normal" ?
                                        this.state.tableData :
                                        [...this.state.tableData.map((e, i) =>
                                            [...e.slice(0, 4),
                                                <Button style={{backgroundColor: "red"}}
                                                        onClick={() => this.delProduct(i)}>
                                                    <Delete/>
                                                </Button>]),
                                            [
                                                <TextField onChange={(e) => this.chData[0] = e.target.value}
                                                           label={"Nome"}/>,
                                                <TextField onChange={(e) => this.chData[1] = e.target.value}
                                                           label={"Codice fiscale"}/>,
                                                <TextField onChange={(e) => this.chData[2] = e.target.value}
                                                           label={"Indirizzo"}/>,
                                                <TextField onChange={(e) => this.chData[3] = e.target.value}
                                                           label={"Note"}/>,
                                                <Button style={{backgroundColor: "green"}}
                                                        onClick={() => this.addProducts()}>
                                                    <Input/>
                                                </Button>
                                            ]]}
                                />
                            </CardBody>
                        </Card>
                    </div>
                    <Footer/>
                </div>
            </div>
        );
    }
}

Clients.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Clients);
