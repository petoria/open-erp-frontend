/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import {Switch, Route, Redirect} from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Navbar from "Dashboard/components/Navbars/Navbar.jsx";
import Footer from "Dashboard/components/Footer/Footer.jsx";
import Sidebar from "Dashboard/components/Sidebar/Sidebar.jsx";
import FixedPlugin from "Dashboard/components/FixedPlugin/FixedPlugin.jsx";

import routes from "Dashboard/routes.js";

import dashboardStyle from "Dashboard/assets/jss/material-dashboard-react/layouts/dashboardStyle.jsx";

import image from "Dashboard/assets/img/sidebar-2.jpg";
import logo from "Dashboard/assets/img/logo_rounded.ico";
import NetworkComponent from "../../NetworkComponent";
import Card from "../../Site/components/Card/Card";
import CardHeader from "../../Site/components/Card/CardHeader";
import CardBody from "../../Site/components/Card/CardBody";
import Table from "../components/Table/Table";
import Button from "../../Site/components/CustomButtons/Button";
import {Add, Close, Delete, Input} from "@material-ui/icons";
import TextField from "@material-ui/core/es/TextField/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {MenuItem} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";


class Departments extends NetworkComponent {
    constructor(props) {
        super(props);
        this.state = {
            image: image,
            color: "blue",
            hasImage: true,
            fixedClasses: "dropdown show",
            mobileOpen: false,
            tableData: [],
            data: [],
            products: [],
            state: "normal"
        };

        this.chData = []
    }

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            const ps = new PerfectScrollbar(this.refs.mainPanel);
        }
        window.addEventListener("resize", this.resizeFunction);
        this.loadState();

        this.GET("/prods")
            .then(res => {
                this.state.products = res.data.list
                return this.GET("/deps")
            }).then(res => {
            console.log(res)
            if (!res.data.list) {
                alert("FUCK");
                return;
            }
            this.setState({
                tableData: res.data.list.map(e => [e.dep_code, e.desc,
                    <Button style={{backgroundColor: "green"}} onClick={() => {
                        this.setState({modal: true, data: e.Productions.map(e => [e.Product.code, e.Product.desc])})
                    }}>Dettagli</Button>])
            })
        })
    }

    addDep() {
        this.POST("/deps", {code: this.chData[0], desc: this.chData[1]}).then(res => {
            this.setState({tableData: [...this.state.tableData, [this.chData[0], this.chData[1]]]})
        });
    }

    delDep(i) {
        this.REQUEST("DELETE", "/deps", {code: this.state.tableData[i][0]}).then(res => {
            this.state.tableData.splice(i, 1);
            this.forceUpdate()
        });
    }

    componentDidUpdate(e) {
        if (e.history.location.pathname !== e.location.pathname) {
            this.refs.mainPanel.scrollTop = 0;
            if (this.state.mobileOpen) {
                this.setState({mobileOpen: false});
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeFunction);
    }

    render() {
        const {classes, ...rest} = this.props;
        return (
            <div className={classes.wrapper}>
                <Sidebar
                    routes={routes}
                    logoText={"PETORIA"}
                    logo={logo}
                    image={this.state.image}
                    handleDrawerToggle={this.handleDrawerToggle}
                    open={this.state.mobileOpen}
                    color={this.state.color}
                    {...rest}
                />
                <div className={classes.mainPanel} ref="mainPanel">
                    <div className={classes.container}>

                        <Card>
                            <CardHeader color="primary">
                                <h4 className={classes.cardTitleWhite}>Unità produttive</h4>
                                <p className={classes.cardCategoryWhite}>
                                    Elenco di tutte le unità produttive
                                </p>
                                <Button style={{backgroundColor: "#ffcc00", color: "black"}} onClick={() => {
                                    this.setState({state: this.state.state === "normal" ? "edit" : "normal"})
                                }}> {this.state.state === "normal" ? "Modifica" : "Salva"} </Button>
                            </CardHeader>
                            <CardBody>
                                <Table
                                    tableHeaderColor="primary"
                                    tableHead={["Codice", "Descrizione", ""]}
                                    tableData={this.state.state === "normal" ?
                                        this.state.tableData :
                                        [...this.state.tableData.map((e, i) =>
                                            [...e.slice(0, 2), <Button onClick={() => {
                                                this.delDep(i);
                                            }}><Delete/></Button>]),
                                            [<TextField onChange={(e) => this.chData[0] = e.target.value}
                                                        label={"Codice"}/>,
                                                <TextField onChange={(e) => this.chData[1] = e.target.value}
                                                           label={"Descrizione"}/>,
                                                <Button onClick={() => this.addDep()}><Input/></Button>]]}
                                />
                            </CardBody>
                        </Card>

                    </div>
                    <Footer/>
                </div>

                <Dialog
                    classes={{
                        root: classes.center,
                        paper: classes.modal
                    }}
                    open={this.state.modal}
                    keepMounted
                    onClose={() => this.setState({modal: false})}>
                    <DialogTitle
                        id="classic-modal-slide-title"
                        disableTypography
                        className={classes.modalHeader}>
                        <Button
                            justIcon
                            className={classes.modalCloseButton}
                            key="close"
                            aria-label="Close"
                            color="transparent"
                            onClick={() => this.setState({modal: false})}
                        >
                            <Close className={classes.modalClose}/>
                        </Button>
                        <h4 className={classes.modalTitle}>Dettagli</h4>
                    </DialogTitle>
                    <DialogContent
                        id="modal-slide-description"
                        className={classes.modalBody}>

                        <Table
                            tableHeaderColor="primary"
                            tableHead={["Codice", "Descrizione", ""]}
                            tableData={[...this.state.data,
                                [
                                    <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}>
                                        <InputLabel
                                            htmlFor="simple-select"
                                            className={classes.selectLabel}>
                                            Seleziona il prodotto da aggiungere
                                        </InputLabel>
                                        <Select
                                            MenuProps={{
                                                className: classes.selectMenu
                                            }}
                                            classes={{
                                                select: classes.select
                                            }}
                                            value={this.state.simpleSelect}
                                            onChange={(e) => this.setState({simpleSelect: e.target.value})}
                                            inputProps={{
                                                name: "simpleSelect",
                                                id: "simple-select"
                                            }}>
                                            {this.state.products.map(e => {
                                                return <MenuItem classes={{
                                                    root: classes.selectMenuItem,
                                                    selected: classes.selectMenuItemSelected
                                                }} value={e.code}>
                                                    {e.desc}
                                                </MenuItem>
                                            })}
                                        </Select>
                                    </FormControl>,

                                    <Button onClick={() => {

                                    }}><Add/></Button>
                                ]]}
                        />

                    </DialogContent>
                </Dialog>
            </div>
        );
    }
}

Departments.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Departments);
