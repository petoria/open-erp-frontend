import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import footerStyle from "Dashboard/assets/jss/material-dashboard-react/components/footerStyle.jsx";
import Favorite from "@material-ui/icons/Favorite";

function Footer({ ...props }) {
  const { classes } = props;
  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <div className={classes.right}>
          &copy; {1900 + new Date().getYear()} , made with{" "}<Favorite className={classes.icon} />{" "}by Petoria for a better future.
        </div>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(footerStyle)(Footer);
