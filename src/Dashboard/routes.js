import Dashboard from "@material-ui/icons/Dashboard";
import AccountBalance from "@material-ui/icons/AccountBalance";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import Add from "@material-ui/icons/AddBox";
import Group from "@material-ui/icons/Group";
import Monetization from "@material-ui/icons/MonetizationOn";
import DashboardPage from "Dashboard/views/Dashboard/Dashboard.jsx";

const dashboardRoutes = [
    {
        path: "/dashboard",
        name: "Dashboard",
        rtlName: "لوحة القيادة",
        icon: Dashboard,
        component: DashboardPage,
        layout: "/admin"
    },
    {
        path: "/deps",
        name: "Dipartimenti",
        rtlName: "لوحة القيادة",
        icon: AccountBalance,
        component: null,
        layout: "/admin"
    },
    {
        path: "/products",
        name: "Prodotti",
        rtlName: "لوحة القيادة",
        icon: ShoppingCart,
        component: null,
        layout: "/admin"
    },
    {
        path: "/clients",
        name: "Clienti",
        rtlName: "لوحة القيادة",
        icon: Group,
        component: null,
        layout: "/admin"
    },
    {
        path: "/orders",
        name: "Ordini cliente",
        rtlName: "لوحة القيادة",
        icon: Monetization,
        component: null,
        layout: "/admin"
    },
    {
        path: "/newOrdine",
        name: "Nuovo ordine",
        rtlName: "التطور للاحترافية",
        icon: Add,
        component: null,
        layout: "/admin"
    },
];

export default dashboardRoutes;