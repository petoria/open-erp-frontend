import {container} from "Site/assets/jss/material-kit-react.jsx";

import imagesStyles from "Site/assets/jss/material-kit-react/imagesStyles.jsx";

const homePageStyle = {

    main: {
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        margin: "-60px 30px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    paper: {
        color: "#7e7d7d",
        padding: 8,
        margin: 2,
        textAlign: "center"
    },

    paper_title: {
        color: "#3bd6ff"
    },

    container: {
        ...container,
        zIndex: "2",
        position: "relative",
        paddingTop: "20vh",
        color: "#FFFFFF"
    },
    container_offborder: {
        marginRight: "auto",
        marginLeft: "auto",
        width: "100%",
        maxWidth: "90%",
        zIndex: "2",
        position: "relative",
        paddingTop: "5vh",
        color: "#FFFFFF"
    },
    cardHidden: {
        opacity: "0",
        transform: "translate3d(0, -60px, 0)"
    },
    pageHeader: {
        minHeight: "100vh",
        height: "auto",
        display: "inherit",
        position: "relative",
        margin: "0",
        padding: "0",
        border: "0",
        alignItems: "center",
        "&:before": {
            background: "rgba(0, 0, 0, 0.5)"
        },
        "&:before,&:after": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: '""'
        },
        "& footer li a,& footer li a:hover,& footer li a:active": {
            color: "#FFFFFF"
        },
        "& footer": {
            position: "absolute",
            bottom: "0",
            width: "100%"
        }
    },
    form: {
        margin: "0"
    },
    cardHeader: {
        width: "auto",
        textAlign: "center",
        marginLeft: "20px",
        marginRight: "20px",
        marginTop: "-40px",
        padding: "20px 0",
        marginBottom: "15px"
    },
    socialIcons: {
        maxWidth: "24px",
        marginTop: "0",
        width: "100%",
        transform: "none",
        left: "0",
        top: "0",
        height: "100%",
        lineHeight: "41px",
        fontSize: "20px"
    },
    divider: {
        marginTop: "30px",
        marginBottom: "0px",
        textAlign: "center"
    },
    cardFooter: {
        paddingTop: "0rem",
        border: "0",
        borderRadius: "6px",
        justifyContent: "center !important"
    },
    socialLine: {
        marginTop: "1rem",
        textAlign: "center",
        padding: "0"
    },
    inputIconsColor: {
        color: "#495057"
    },
    textCentered: {
        textAlign: "center"
    },
    gridItemCentered: {
        marginLeft: "auto !important",
        marginRight: "auto !important"
    },
    marginTopBottom: {
        marginTop: "2%",
        marginBottom: "2%"
    },
    leftAlign: {
        textAlign: "left"
    },
    leftMargin2: {
        marginLeft: "2%"
    },
    leftMargin10: {
        marginLeft: "10%"
    },
    rightAlign: {
        textAlign: "right"
    },
    rightMargin2: {
        marginRight: "2%"
    },
    rightMargin10: {
        marginRight: "10%"
    },
    verticalCentered: {
        display: "flex",
        alignItems: "center"
    },
    floatRight: {
        float: "right"
    },
    roundedbox: {
        borderRadius: "6px"
    },
    ...imagesStyles
};

export default homePageStyle;
