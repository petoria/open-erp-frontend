import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

import Button from "Site/components/CustomButtons/Button.jsx";

import headerLinksStyle from "Site/assets/jss/material-kit-react/components/headerLinksStyle.jsx";

function HeaderLinks({...props}) {
    const {classes, history} = props;
    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <Button
                    onClick={() =>history.push("/chi-siamo")}
                    color="transparent"
                    className={classes.navLink}
                >Chi siamo</Button>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Button
                    onClick={() => history.push("/licenze")}
                    color="transparent"
                    className={classes.navLink}
                >Licenze</Button>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Button
                    onClick={() => history.push("/pricing")}
                    color="transparent"
                    className={classes.navLink}
                >Prezzi</Button>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Button
                    onClick={() => history.push("/login")}
                    color="transparent"
                    className={classes.navLink}
                >
                    {!window.isLogged && <i className={"fas fa-sign-in-alt"}/>}
                    {window.isLogged && <i className={"fas fa-user-circle"}/>}
                </Button>
            </ListItem>
        </List>
    );
}

export default withStyles(headerLinksStyle)(HeaderLinks);
