import React from "react";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Header from "Site/components/Header/Header.jsx";
import Footer from "Site/components/Footer/Footer.jsx";
import Button from "Site/components/CustomButtons/Button.jsx";
import HeaderLinks from "Site/components/Header/HeaderLinks.jsx";
import Parallax from "Site/components/Parallax/Parallax.jsx";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import NetworkComponent from "../../../NetworkComponent";

import homePageStyle from "Site/assets/jss/material-kit-react/views/homePage.jsx";

import backgrund from "Site/assets/img/Pricing/bg.jpg";

class PricingPage extends NetworkComponent {

    state = {
        plans: [
            {
                title: "free",
                users: 3,
                cods: 10,
                ram: "512 MB",
                cpu: "1%",
                staticServe: false,
                size: "0 MB",
                database: "sqlite",
                assistenza: false,
                price: "EUR 0.00"
            },
            {
                title: "I",
                users: 5,
                cods: 50,
                ram: "1024 MB",
                cpu: "3%",
                staticServe: false,
                size: "0 MB",
                database: "sqlite",
                assistenza: true,
                price: "EUR 2.00"
            },
            {
                title: "II",
                users: 10,
                cods: 100,
                ram: "1536 MB",
                cpu: "5%",
                staticServe: true,
                size: "100 MB",
                database: "sqlite",
                assistenza: true,
                price: "EUR 5.00"
            },
            {
                title: "III",
                users: 50,
                cods: 500,
                ram: "4096 MB",
                cpu: "10%",
                staticServe: true,
                size: "1024 MB",
                database: "sqlite",
                assistenza: true,
                price: "EUR 15.00"
            },
            {
                title: "IV",
                users: 100,
                cods: 1000,
                ram: "8192 MB",
                cpu: "1 core",
                staticServe: true,
                size: "15 GB",
                database: "mysql",
                assistenza: true,
                price: "EUR 50.00"
            },
            {
                title: "V",
                users: "no limit",
                cods: "no limit",
                ram: "16 GB",
                cpu: "2 core",
                staticServe: true,
                size: "15 GB",
                database: "mysql",
                assistenza: true,
                price: "EUR 150.00"
            },
            {
                title: "INSTALL",
                users: "no limit",
                cods: "no limit",
                ram: "no limit",
                cpu: "no limit",
                staticServe: true,
                size: "no limit",
                database: "any compatible",
                assistenza: true,
                price: "Contact us"
            },
            {
                title: "GIT",
                users: "no limit",
                cods: "no limit",
                ram: "no limit",
                cpu: "no limit",
                staticServe: true,
                size: "no limit",
                database: "any compatible",
                assistenza: false,
                price: "EUR 0.00"
            }
        ],
        selplan: -1
    };

    select(i){
        this.setState({selplan: i})
    }

    render() {
        const {classes, ...rest} = this.props;
        const imageClasses = classNames(
            classes.imgRaised,
            classes.imgRoundedCircle,
            classes.imgFluid
        );
        const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
        return (
            <div>
                <Header
                    color="transparent"
                    rightLinks={<HeaderLinks history={this.props.history}/>}
                    fixed
                    changeColorOnScroll={{
                        height: 200,
                        color: "white"
                    }}
                    {...rest}
                />

                <Parallax image={backgrund}>
                    <div className={classes.container_offborder}>
                        <Grid container justify="center" alignItems="center">
                            {this.state.plans.map((e, i) => {
                                return <Grid item xs={12} md>
                                    <Paper className={classes.paper}>
                                        <h2 className={classes.paper_title}><b>{e.title}</b></h2>
                                        <p>UTENTI<br/><b>{e.users}</b></p>
                                        <p>MASSIMO COD. MAGAZZINO<br/><b>{e.cods}</b></p>
                                        <p>RAM<br/><b>{e.ram}</b></p>
                                        <p>CPU<br/><b>{e.cpu}</b></p>
                                        <p>STATIC SERVE<br/><h3>{e.staticServe ? <i className="fas fa-check" style={{color: "green"}}/> : <i className="fas fa-times" style={{color: "green"}}/>}</h3></p>
                                        <p>SERVE SIZE<br/><b>{e.size}</b></p>
                                        <p>DATABASE<br/><b>{e.database}</b></p>
                                        <p>ASSISTENZA<br/><h3>{e.assistenza ? <i className="fas fa-check" style={{color: "green"}}/> : <i className="fas fa-times" style={{color: "green"}}/>}</h3></p>
                                        <Button color="primary" round onClick={() => this.select(i)}>
                                            {e.price}
                                        </Button>
                                    </Paper>
                                </Grid>
                            })}
                        </Grid>
                    </div>
                </Parallax>

                {this.state.selplan > -1 && <div className={classes.main + " " + classes.mainRaised}>
                    PAY BITCH
                </div>}


                <Footer/>
            </div>
        );
    }
}

export default withStyles(homePageStyle)(PricingPage);