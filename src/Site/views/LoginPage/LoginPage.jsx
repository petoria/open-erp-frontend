import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment/index";
import Icon from "@material-ui/core/Icon/index";
import People from "@material-ui/icons/People";
import Header from "Site/components/Header/Header.jsx";
import HeaderLinks from "Site/components/Header/HeaderLinks.jsx";
import Footer from "Site/components/Footer/Footer.jsx";
import GridContainer from "Site/components/Grid/GridContainer.jsx";
import GridItem from "Site/components/Grid/GridItem.jsx";
import Button from "Site/components/CustomButtons/Button.jsx";
import Card from "Site/components/Card/Card.jsx";
import CardBody from "Site/components/Card/CardBody.jsx";
import CardHeader from "Site/components/Card/CardHeader.jsx";
import CardFooter from "Site/components/Card/CardFooter.jsx";
import CustomInput from "Site/components/CustomInput/CustomInput.jsx";

import loginPageStyle from "Site/assets/jss/material-kit-react/views/loginPage.jsx";

import {Grid, Link} from "@material-ui/core";
import SnackbarContent from "../../components/Snackbar/SnackbarContent";
import NetworkComponent from "../../../NetworkComponent";
import NavPills from "../../components/NavPills/NavPills";
import CustomLinearProgress from "../../components/CustomLinearProgress/CustomLinearProgress";
import DialogTitle from "@material-ui/core/DialogTitle/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import DialogActions from "@material-ui/core/DialogActions/index";
import Dialog from "@material-ui/core/Dialog/index";
import Transition from "react-transition-group/Transition";

import bg from "Site/assets/img/Login/bg.jpg";

class LoginPage extends NetworkComponent {
    constructor(props) {
        super(props);

        this.state = {
            cardAnimaton: "cardHidden",
            login: true
        };

        this.username = "stefano";
        this.pass = "";
        this.mail = "";
    }

    componentDidMount() {
        setTimeout(
            function () {
                this.setState({cardAnimaton: ""});
            }.bind(this),
            700
        );

    }

    enterYourLifeBro(event) {
        let key = event.keyCode || event.which;
        if (key === 13) {
            this.login();
        }
    }

    login() {
        if (this.state.login)
            this.POST("/login", {username: this.username, password: this.pass}, {}, false)
                .then(r => {
                    if (r.code !== 200) {
                        this.setState({
                            notification: <SnackbarContent
                                message={<span>{r.data.status}</span>}
                                close
                                color="danger"
                            />
                        });
                        return;
                    }

                    this._saveState(r.data.token, this.username, r.data.sessions, true);
                    window.sessions = r.data.sessions;
                    this.forceUpdate()

                });
        else {
            this.setState({
                notification: <CustomLinearProgress
                    variant="indeterminate"
                    color="primary"
                />
            });
            this.POST("/new_user", {username: this.username, password: this.pass, email: this.mail}, {}, false)
                .then(r => {
                    if (r.code === 200)
                        this.setState({
                            login: true, notification: <SnackbarContent
                                message={<span>Conferma il tuo indirizzo email e inizia a lavorare!</span>}
                                close
                                color="info"
                            />
                        });
                    else
                        this.setState({
                            login: true, notification: <SnackbarContent
                                message={<span>{r.data.status}</span>}
                                close
                                color="danger"
                            />
                        });
                })
        }
    }

    loginAPI(session) {
        this.POST("/login", {username: this.username, password: this.pass, session: session}, {}, false)
            .then(r => {
                if (r.code !== 200) return;

                this._saveStateAPI(r.data.token, r.data.dns, r.data.port);
                return this.GET("/hello");
            })
            .then((check) => {
                //aggiunto da vezzo mentre era in coma, da controllare :)
                if (!check) {
                    this._saveState("", "", [], false);
                    this.forceUpdate();

                    return;
                }
                //fine delle cose aggiunte da vezzo mentre era in coma :p

                if (check.data.msg !== "PETORIA") this.props.history.push("/");
                else {
                    //riga commentata da [vedi riga 124, non c'ió voglia di riscrivere]
                    // window.isLogged = false;

                    this.props.history.push("/admin/dashboard")
                }
            });
    }

    invite(instanceID) {
        this.setState({invWait: true});
        this.POST("/invite", {username: this.username, session: instanceID})
            .then(r => {
                this.setState({invWait: false, inviteUser: false});
            })
    }

    render() {
        const {classes, ...rest} = this.props;
        return (
            <div>
                <Header
                    absolute
                    color="transparent"
                    rightLinks={<HeaderLinks history={this.props.history}/>}
                    fixed
                    changeColorOnScroll={{
                        height: 200,
                        color: "white"
                    }}
                    {...rest}
                />

                <div
                    className={classes.pageHeader}
                    style={{
                        backgroundImage: "url(" + bg + ")",
                        backgroundSize: "cover",
                        backgroundPosition: "top center"
                    }}
                >
                    <div className={classes.container}>
                        {!window.isLogged && <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={4}>
                                <Card className={classes[this.state.cardAnimaton]}>
                                    <form className={classes.form}>
                                        <CardHeader color="primary" className={classes.cardHeader}>
                                            <h4>Login</h4>
                                        </CardHeader>
                                        <CardBody>
                                            {this.state.notification}
                                            <CustomInput
                                                labelText="Username"
                                                id="user"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: (e) => {
                                                        this.username = e.target.value
                                                    },
                                                    type: "text",
                                                    onKeyPress: this.enterYourLifeBro.bind(this),
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <People className={classes.inputIconsColor}/>
                                                        </InputAdornment>
                                                    )
                                                }}
                                            />
                                            <CustomInput
                                                labelText="Password"
                                                id="pass"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: (e) => {
                                                        this.pass = e.target.value
                                                    },
                                                    type: "password",
                                                    onKeyPress: this.enterYourLifeBro.bind(this),
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <Icon className={classes.inputIconsColor}>
                                                                lock_outline
                                                            </Icon>
                                                        </InputAdornment>
                                                    )
                                                }}
                                            />
                                            {!this.state.login && [
                                                <CustomInput
                                                    labelText="email"
                                                    id="pass"
                                                    formControlProps={{
                                                        fullWidth: true
                                                    }}
                                                    inputProps={{
                                                        onChange: (e) => {
                                                            this.mail = e.target.value
                                                        },
                                                        type: "email",
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <Icon className={classes.inputIconsColor}>
                                                                    email
                                                                </Icon>
                                                            </InputAdornment>
                                                        )
                                                    }}
                                                />,
                                                <p>Verrá inviato un link di conferma all'indirizzo selezionato</p>]}
                                        </CardBody>
                                        <CardFooter className={classes.cardFooter}>
                                            <GridContainer>
                                                <GridItem>
                                                    <Button simple color="primary" size="lg"
                                                            onClick={() => this.login()}>
                                                        {this.state.login ? "Login" : "Sign Up"}
                                                    </Button>
                                                </GridItem>
                                                <GridItem>
                                                    <Button simple color="primary" size="lg" onClick={() => {
                                                        this.setState({login: !this.state.login})
                                                    }}>
                                                        {this.state.login ? "Registrati" : "Torna al login"}
                                                    </Button>
                                                </GridItem>
                                            </GridContainer>
                                        </CardFooter>
                                    </form>
                                </Card>
                            </GridItem>
                        </GridContainer>}

                        {window.isLogged &&
                        <GridItem xs={12} sm={12} md={12}>
                            <Card className={classes[this.state.cardAnimaton]}>
                                <CardBody>
                                    <NavPills
                                        color="primary"
                                        horizontal={{
                                            tabsGrid: {xs: 12, sm: 4, md: 2},
                                            contentGrid: {xs: 12, sm: 8, md: 10}
                                        }}
                                        tabs={

                                            window.sessions && window.sessions.map(e => {
                                                return {
                                                    tabButton: e.name,
                                                    tabContent: (
                                                        <Grid container>
                                                            <Grid item xs={12}>{e.desc}</Grid>
                                                            <Grid item xs={8}>Located: {e.loc}</Grid>
                                                            <Grid xs={4}>
                                                                <Button
                                                                    className={classes.green}
                                                                    onClick={() => this.loginAPI(e.id)}>LOGIN</Button>
                                                                <Button className={classes.blue}
                                                                        onClick={() => this.setState({
                                                                            inviteUser: true,
                                                                            instance: e.name,
                                                                            iid: e.id
                                                                        })}>INVITA</Button>
                                                            </Grid>

                                                        </Grid>)
                                                }
                                            })
                                        }
                                    />
                                </CardBody>

                                <Button className={classes.red}
                                        onClick={() => {
                                            this._saveState("", "", [], false);
                                            this.forceUpdate();
                                        }}>Logout</Button>
                            </Card>
                        </GridItem>}

                    </div>
                    <Footer whiteFont/>


                    {/*    modals   */}

                    <Dialog
                        classes={{
                            root: classes.center,
                            paper: classes.modal
                        }}
                        open={this.state.inviteUser}
                        TransitionComponent={Transition}
                        keepMounted
                        onClose={() => this.setState({inviteUser: false})}
                        aria-labelledby="classic-modal-slide-title"
                        aria-describedby="classic-modal-slide-description"
                    >
                        <DialogTitle
                            id="classic-modal-slide-title"
                            disableTypography
                            className={classes.modalHeader}
                        >
                            <h4 className={classes.modalTitle}>Invita un utente a partecipare
                                all'istanza {this.state.instance}</h4>
                        </DialogTitle>
                        <DialogContent
                            id="classic-modal-slide-description"
                            className={classes.modalBody}>

                            <CustomInput
                                labelText="Username"
                                id="user"
                                formControlProps={{
                                    fullWidth: true
                                }}
                                inputProps={{
                                    onChange: (e) => {
                                        this.username = e.target.value
                                    },
                                    type: "text",
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <People className={classes.inputIconsColor}/>
                                        </InputAdornment>
                                    )
                                }}
                            />

                            {this.state.invWait && <CustomLinearProgress
                                variant="indeterminate"
                                color="primary"
                            />}

                        </DialogContent>
                        <DialogActions className={classes.modalFooter}>
                            <Button color="transparent" default onClick={() => {
                                this.invite(this.state.iid)
                            }}>
                                OK
                            </Button>
                            <Button
                                onClick={() => this.setState({inviteUser: false})}
                                color="danger"
                                simple>
                                Annulla
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>

            </div>
        );
    }
}

export default withStyles(loginPageStyle)(LoginPage);
