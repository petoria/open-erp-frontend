import React from "react";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Header from "Site/components/Header/Header.jsx";
import Footer from "Site/components/Footer/Footer.jsx";
import HeaderLinks from "Site/components/Header/HeaderLinks.jsx";
import Parallax from "Site/components/Parallax/Parallax.jsx";
import Grid from "@material-ui/core/Grid/index";
import NetworkComponent from "../../../NetworkComponent";

import homePageStyle from "Site/assets/jss/material-kit-react/views/homePage.jsx";

import RightCard from "./Components/RightCard";
import LeftCard from "./Components/LeftCard";

import bg from "Site/assets/img/ChiSiamo/bg.jpg";
import logoBig from "Site/assets/img/ChiSiamo/logoBig.png";
import abdulkadir from "Site/assets/img/ChiSiamo/Faces/RamyAbdulkadir.jpg";
import fontana from "Site/assets/img/ChiSiamo/Faces/StefanoFontana.jpg";
import frusca from "Site/assets/img/ChiSiamo/Faces/LorenzoFrusca.jpg";
import pacecchi from "Site/assets/img/ChiSiamo/Faces/PaoloPacecchi.jpg";
import sciola from "Site/assets/img/ChiSiamo/Faces/LorenzoSciola.jpg";
import torli from "Site/assets/img/ChiSiamo/Faces/FrancescoTorli.jpg";
import vezzoli from "Site/assets/img/ChiSiamo/Faces/LucaVezzoli.jpg";

class ChiSiamoPage extends NetworkComponent {

    constructor(props) {
        super(props);

        this.state = {
            people: [
                {
                    surname: "Abdulkadir",
                    name: "Ramy",
                    desc: "Uno sviluppatore frontend con uno stile discutibile",
                    img: abdulkadir
                },
                {
                    surname: "Fontana",
                    name: "Stefano",
                    desc: "Uno sviluppatore backend sempre pronto a sviluppare codice",
                    img: fontana
                },
                {
                    surname: "Frusca",
                    name: "Lorenzo",
                    desc: "Un tester che stranamente va d'accordo con gli sviluppatori",
                    img: frusca
                },
                {
                    surname: "Pacecchi",
                    name: "Paolo",
                    desc: "Addetto alle relazioni esterne per mantenere un rapporto cliente-sviluppatori sicuro",
                    img: pacecchi
                },
                {
                    surname: "Sciola",
                    name: "Lorenzo",
                    desc: "Un altro sviluppatore frontend con uno stile discutibile",
                    img: sciola
                },
                {
                    surname: "Torli",
                    name: "Francesco",
                    desc: "Un tester che va d'accordo solo con gli sviluppatori backend",
                    img: torli
                },
                {
                    surname: "Vezzoli",
                    name: "Luca",
                    desc: "Uno strano sviluppatore backend che preferisce fare frontend",
                    img: vezzoli
                },
            ]
        };
    }

    render() {
        const {classes, ...rest} = this.props;
        const imageClasses = classNames(
            classes.imgRaised,
            classes.imgRoundedCircle,
            classes.imgFluid
        );
        const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);

        return (
            <div>
                <Header
                    color="transparent"
                    rightLinks={<HeaderLinks history={this.props.history}/>}
                    fixed
                    changeColorOnScroll={{
                        height: 200,
                        color: "white"
                    }}
                    {...rest}
                />

                <Parallax image={bg}>
                    <div className={classes.container}>
                        <Grid container>
                            <Grid item xs={8}>
                                <div className={classes.brand}>
                                    <h1 className={classes.title}><strong>Petoria</strong></h1>
                                    <h3 className={classes.subtitle}>
                                        Descrizione
                                    </h3>
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <img
                                    src={logoBig}
                                    height="160"
                                    className={classes.imgRounded + " " + classes.imgFluid}
                                    alt="...."
                                />
                            </Grid>
                        </Grid>
                    </div>
                </Parallax>

                <Grid container justify="center" alignItems="center">
                    <Grid item xs={9}>
                        <div className={classNames(classes.main, classes.mainRaised)}>
                            <Grid container justify="center" alignItems="center">
                                {this.state.people.map((e, i) => {
                                    if (i % 2 === 0)
                                        return <Grid item xs={12} className={classes.roundedbox}>
                                            <div className={classes.textCentered}>
                                                <LeftCard
                                                    surname={e.surname}
                                                    name={e.name}
                                                    desc={e.desc}
                                                    img={e.img}
                                                />
                                            </div>
                                        </Grid>;
                                    else
                                        return <Grid item xs={12} className={classes.roundedbox} style={{backgroundColor: "#E9E9E9"}}>
                                            <div className={classes.textCentered}>
                                                <RightCard
                                                    surname={e.surname}
                                                    name={e.name}
                                                    desc={e.desc}
                                                    img={e.img}
                                                />
                                            </div>
                                        </Grid>;
                                })}
                            </Grid>
                        </div>
                    </Grid></Grid>

                <Footer/>
            </div>
        );
    }
}

export default withStyles(homePageStyle)(ChiSiamoPage);