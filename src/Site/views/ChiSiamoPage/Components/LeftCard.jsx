import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";

import homePageStyle from "Site/assets/jss/material-kit-react/views/homePage.jsx";

import Grid from "@material-ui/core/Grid/index";

class LeftCard extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.gridItemCentered + "  " + classes.marginTopBottom}>
                <Grid container>
                    <Grid item xs={2} className={classes.leftAlign}>
                        <div className={classes.leftMargin10}>
                            <img alt="...." src={this.props.img} className={classes.imgRounded + " " + classes.imgFluid}
                                 height={this.props.height} width={this.props.width}/>
                        </div>
                    </Grid>
                    <Grid item xs={10} className={classes.leftAlign}>
                        <div className={classes.leftMargin2 + " " + classes.verticalCentered} style={{height: "100%"}}>
                            <div>
                                <h3 style={{color: "#99CCFF", padding: 0, margin: 0}}>
                                    <strong>{(this.props.surname).toUpperCase()}</strong> {(this.props.name).charAt(0).toUpperCase()}{(this.props.name).substr(1, (this.props.name).length - 1).toLowerCase()}
                                </h3>
                                <i>{this.props.desc}</i>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(homePageStyle)(LeftCard);
