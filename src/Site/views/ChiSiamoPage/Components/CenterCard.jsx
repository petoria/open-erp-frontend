import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";

import homePageStyle from "Site/assets/jss/material-kit-react/views/homePage.jsx";

import Grid from "@material-ui/core/Grid/index";

class CenterCard extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.gridItemCentered + "  " + classes.marginTopBottom}>
                <Grid container justify="center" alignItems="center">
                    <Grid item xs={12}>
                        <img alt="...." src={this.props.img} className={classes.imgRounded + " " + classes.imgFluid}
                             height={this.props.height} width={this.props.width}/>
                    </Grid>
                    <Grid item xs={12}>
                        <h3 style={{color: "lightblue"}}><strong>{this.props.name}</strong></h3>
                        <i>{this.props.desc}</i>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(homePageStyle)(CenterCard);
