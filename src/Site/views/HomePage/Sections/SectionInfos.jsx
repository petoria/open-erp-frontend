import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid/index";

import infosStyle from "Site/assets/jss/material-kit-react/views/componentsSections/infosStyle.jsx";

class SectionInfos extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.section}>
                <div className={classes.container}>
                    <div>
                        <Grid container>
                            <Grid item xs={12} sm={4}>
                                <div className={classes.textCentered}>
                                    <i className={"fas fa-shield-alt fa-10x"}/>
                                    <br/>
                                    <h1>SICURO</h1>
                                    <div className={classes.light}>
                                        Un'infrastruttura studiata<br/>
                                        per mantenere separazione<br/>
                                        e sicurezza negli ambienti<br/>
                                        personali.
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <div className={classes.textCentered}>
                                    <i className={"fas fa-user fa-10x"}/>
                                    <br/>
                                    <h1>INTUITIVO</h1>
                                    <div className={classes.light}>
                                        UI studiata per facilitare<br/>
                                        le operazioni.
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <div className={classes.textCentered}>
                                    <i className={"fas fa-tachometer-alt fa-10x"}/>
                                    <br/>
                                    <h1>VELOCE</h1>
                                    <div className={classes.light}>
                                        Istanze separate, macchine<br/>
                                        performanti e processi<br/>
                                        ottimizzati per una reattività<br/>
                                        immediata.
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div className={classes.space50}/>
                </div>
            </div>
        );
    }
}

export default withStyles(infosStyle)(SectionInfos);
