import React from "react";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Header from "Site/components/Header/Header.jsx";
import Footer from "Site/components/Footer/Footer.jsx";
import HeaderLinks from "Site/components/Header/HeaderLinks.jsx";
import Parallax from "Site/components/Parallax/Parallax.jsx";
import NetworkComponent from "../../../NetworkComponent";
import Grid from "@material-ui/core/Grid/index";

import homePageStyle from "Site/assets/jss/material-kit-react/views/homePage.jsx";

import SectionInfos from "./Sections/SectionInfos";

import homebg from "Site/assets/img/Home/bg.jpg";
import logoBig from "Site/assets/img/Home/logoBig.png";

class HomePage extends NetworkComponent {

    render() {
        const {classes, ...rest} = this.props;
        const imageClasses = classNames(
            classes.imgRaised,
            classes.imgRoundedCircle,
            classes.imgFluid
        );
        const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);

        return (
            <div>
                <Header
                    color="transparent"
                    rightLinks={<HeaderLinks history={this.props.history}/>}
                    fixed
                    changeColorOnScroll={{
                        height: 200,
                        color: "white"
                    }}
                    {...rest}
                />

                <Parallax image={homebg}>
                    <div className={classes.container}>
                        <Grid container>
                            <Grid item xs={8}>
                                <div className={classes.brand}>
                                    <h1 className={classes.title}><strong>open-erp</strong></h1>
                                    <h3 className={classes.subtitle}>
                                        La soluzione opensource per un miglior futuro
                                    </h3>
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <img
                                    src={logoBig}
                                    height="160"
                                    className={classes.imgRounded + " " + classes.imgFluid}
                                    alt="...."
                                />
                            </Grid>
                        </Grid>
                    </div>
                </Parallax>

                <div className={classNames(classes.main, classes.mainRaised)}>
                    <SectionInfos/>
                </div>

                <Footer/>
            </div>
        );
    }
}

export default withStyles(homePageStyle)(HomePage);
